﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    public Transform target;
    public float smoothness;
    public float offset = 4;
    public Vector2 clamp;

    void LateUpdate()
    {
        Vector3 camPos = transform.position;

        camPos.x = target.position.x + offset;

        camPos.x = Mathf.Clamp(camPos.x, clamp.x, clamp.y);

        transform.position = Vector3.Lerp(transform.position, camPos, smoothness);
    }
}
