﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int speed;

    private void Update()
    {
        Movement();
    }

    private void Movement()
    {
        float x = Input.GetAxisRaw("Horizontal");

        if(x > 0 || x < 0)
        {
            Vector3 scale = transform.localScale;
            scale.x = x;
            transform.localScale = scale;
        }

        float y = Time.deltaTime;

        transform.Translate(new Vector3(x, 0, 0).normalized * speed * Time.deltaTime);
    }
}
