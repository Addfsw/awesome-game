﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour
{
    public Transform teleportPoint;
    public Vector2 clampForCamera;

    public Transition secondTransition;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.CompareTag("Player"))
        {
            Camera.main.GetComponent<LookAtTarget>().clamp = clampForCamera;

            collision.transform.position = secondTransition.teleportPoint.position;
        }
    }
}
